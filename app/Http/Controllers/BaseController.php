<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class BaseController extends Controller
{
    //
    public function index()
    {
        $results = DB::select('CALL sp_example()');

        // ประมวลผลผลลัพธ์ที่ได้รับจาก Stored Procedure ตามความเหมาะสม
        // ตัวอย่างการแสดงผลลัพธ์
        foreach ($results as $result) {
            $resultArray = (array) $result;

            // ทำอะไรกับข้อมูลที่ได้รับ
            $n1 = $resultArray['name'];
            $n2 = $resultArray['email'];
            $n3 = $resultArray['created_at'];
        }
        $data = ['name' => $n1] + ['email' => $n2] + ['created_at' => $n3];
        // คืนค่าผลลัพธ์หรือกำหนดค่าอื่นๆ ตามความต้องการ
        return view('baseindex', $data);
    }
}